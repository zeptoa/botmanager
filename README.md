# botmanager is a gitlab notification tool for telegram 

This is an initial project. This project is purly based on php and codeignitor framework - stay connected while we upgrade!!!

### Composer requirements (telegram bot API) ###
~~~
{
  "require": {
    "unreal4u/telegram-api": "~3.4"
  }
}
~~~

**Change your bot token created using botfather in telegram. You can refer follwing link more information to setup bot father in telegram**

View more 
https://core.telegram.org/bots/#6-botfather

To create a new bot use https://t.me/botfather

**Create a webhook in telegram by visiting the following link in any browser.** 

Update token and domain in the URL before seting webhook in telegram 

Add your token after word **bot** and do not use any brackets
 
E.g.: bot**123456789ABCDEFGHIJKLMNOPQRSTUWXYZ**

~~~
https://api.telegram.org/bot{Your-bot-token-generated-by-bot-father}/setWebhook?url=https://yourdomain/telegram
~~~

**Update bot token in file /application/libraries/Telegramreceiver.php**/

~~~
define('BOT_TOKEN', 'Your-bot-token-generated-by-bot-father');
~~~

### Upload Database ###

You can see botmanager(1).sql in root folder . Create a database and upload the file to your MYSQL database . Update your /application/config/database.php file to change the database settings.

~~~
'hostname' => 'localhost',
'username' => 'root',
'password' => '',
'database' => 'botmanager',
~~~

Enjoy!!



