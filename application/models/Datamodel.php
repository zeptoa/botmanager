<?php
class Datamodel extends CI_Model {

    public function count_users($chatid)
    {
          $this->load->database();
          $this->db->where('chatid',$chatid);    
          $this->db->from('users');
          return $this->db->count_all_results();          
          
    }
    
     public function gethookid($chatid)
    {
        
        $this->load->database();
        $this->db->where("chatid",$chatid);       
        $query = $this->db->get("users");
        $rslt = $query->result();
        return $rslt[0]->hookid;  
        
    }
    
    public function getchatid($hook)
    {   
        $this->load->database();
        $this->db->where("hookid",$hook);       
        $query = $this->db->get("users");
        $rslt = $query->result();
        return $rslt ; 
    }
    
    public function addlog($r)
    {
        $this->load->database();
      $chatlog = array(
                'chatid ' => $r['chatid'],
                'personid ' => $r['personid'],
                'message' => $r['message'],
                'btn' => $r['btn'],
                'samayam ' => date("Y-m-d H:i:s")
            );       
            $this->db->insert('chatlog', $chatlog);
    
    }
    
    
     public function adduser($r)
    {
    
        $hookid = date('ymdHisu'). $r['chatid'];
    
        $chatusers = array(
                'chatid ' => $r['chatid'],
                'hookid ' =>$hookid,
                'joindate' => date("Y-m-d"),
                'status' => 'A'
                
        );       
        $this->db->insert('users', $chatusers);
    
    }
    
}