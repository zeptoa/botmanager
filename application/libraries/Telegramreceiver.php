<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//require_once( './vendor/autoload.php');
include './vendor/autoload.php';

define('BOT_TOKEN', '');


use React\EventLoop\Factory;
use unreal4u\TelegramAPI\HttpClientRequestHandler;
use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;
use unreal4u\TelegramAPI\Telegram\Types\KeyboardButton;
use unreal4u\TelegramAPI\Telegram\Types\Inline\Keyboard\Markup;
use unreal4u\TelegramAPI\TgLog;

class Telegramreceiver
{

    function start()
    {
        echo 'working' ;
    }

    public function getbasic($update)
    {
        $tx = '';
        $btnrslt = '';
        $chatID = '';
        $person  = '';
        if(isset($update["message"]["chat"]["type"]))
        {
        $group = $update["message"]["chat"]["type"] ;
        } else if(isset($update['callback_query']["message"]["chat"]["type"]))
        {
        $group = $update['callback_query']["message"]["chat"]["type"] ;
        }
        else
        {
        $group ="N" ;
        }
        if(isset($update['message']) ) 
        {
              $chatID = $update["message"]["chat"]["id"];
              if(isset($update["from"]["first_name"]))
              {
              $person = $update["from"]["first_name"];
              }else if (isset($update["message"]["chat"]["first_name"]))
              {
              $person = $update["message"]["chat"]["first_name"];
              }
              else
              {
              $person ="Unknown"  ;
              }
              $tx = $update["message"]["text"];
              if(isset($update["message"]["from"]["username"]))
              {
              $uid =  $update["message"]["from"]["username"];
              }else if(isset($update["from"]["username"])) 
              {
              $uid = $update["from"]["username"] ; 
              }  
              else
              {
              $uid = $chatID ;
              }    
      }

      $result = array('chatid'=>$chatID,'personid'=>$person,'message'=>$tx,'btn'=>$btnrslt);
      return $result ;

    }
    public function sendreply($txt,$chatID,$person,$inline)
    {
        $loop = Factory::create();
        $tgLog = new TgLog(BOT_TOKEN, new HttpClientRequestHandler($loop));
        $sendMessage = new SendMessage();
        $sendMessage->chat_id = $chatID;
        $sendMessage->text =  $txt;
        $sendMessage->disable_web_page_preview = false;
        $sendMessage->parse_mode = 'HTML';

        if($inline != Null)
        {
        $sendMessage->reply_markup = $inline ;
        }
        
        $tgLog->performApiRequest($sendMessage);
        $loop->run();

    }


 


}
