<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gitlab extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		  echo 'not allowed ' ;  
	}
  
  public function webhook($hook)
  {
        
        $content = file_get_contents("php://input");
        $update = json_decode($content, true);
        $this->load->database();
        $this->load->model('Datamodel'); 
        $this->load->library('telegramreceiver');
                        
        $project = '';
        $giturl= '';
        $typ = '' ;
        $user = '' ;
        $title = '';
        $commit = '';
        
        
        
        $res = $this->Datamodel->getchatid($hook);
        if(count($res)>0)
        {
          $chatid  = $res[0]->chatid;
          if(isset($update["object_kind"]))
          {         
          
            //for git lab webhook referance visit https://docs.gitlab.com/ee/user/project/integrations/webhooks.html
          
            $typ = $update["object_kind"] ;
            $project = $update["project"]["name"];
            $giturl = '<a href= "'.$update["project"]["git_http_url"].'">Project URL</a>'; 
            
            
            
            switch ($typ ) 
            {
                case "push":
                  
                  $user = $update["user_name"];
                  $commit = $update["total_commits_count"];  
                  $commitsarray = $update["commits"];
                  $commitsection ='Commits Details :';
                   
                  foreach ($commitsarray as $row)
                   {
                    $commitsection = $commitsection.PHP_EOL.'<a href = "'.$row["url"].'">'.$row["title"].'</a>'.PHP_EOL ; 
                   }          
                         
                  $reply = ( "<b>". $user."</b> Pushed  to ".$project.PHP_EOL.$giturl.PHP_EOL.PHP_EOL.$commitsection.PHP_EOL.'<b>Total commits : '. $commit.'</b>') ;
                  $this->telegramreceiver->sendreply($reply,$chatid ,"",null);
                  
                break;
                
                case "merge_request":                                    
                
                  $user = $update["user"]["name"];  
                  $title = $update["object_attributes"]["title"];                                    
                  $mergeurl = $update["object_attributes"]["url"];
                  $state = $update["object_attributes"]["state"]; 
                  $reply = ( "<b>".$user."</b> ".$state." Merge request for ".$project.PHP_EOL.$giturl.PHP_EOL.PHP_EOL.'<b>Title : </b>'.$title.PHP_EOL.'Merge request  : '. $mergeurl.PHP_EOL.PHP_EOL.'<b>State : </b>'. $state) ;
                  $this->telegramreceiver->sendreply($reply,$chatid ,"",null);
                  
                break;
                
                case "issue":                                    
                
                  $user = $update["user"]["name"];  
                  $title = $update["object_attributes"]["title"];        
                  $state = $update["object_attributes"]["state"]; 
                  $desc = $update["object_attributes"]["description"];
                  $issueurl = $update["object_attributes"]["url"]; 
                  
                  if(strlen($desc) > 50 )
                  {
                     $desc = substr($desc,0,50).'...';
                  }                                                     
                      
                  $reply = ( "<b>".$user."</b> ".$state." an issue :".PHP_EOL.'Title:'.$title.PHP_EOL.PHP_EOL.$issueurl.PHP_EOL.PHP_EOL.'State : '. $state.PHP_EOL.PHP_EOL.$desc.PHP_EOL.PHP_EOL.$giturl) ;
                  $this->telegramreceiver->sendreply($reply,$chatid ,"",null);
                  
                break;
                
                 
                case "tag_push":                                    
                
                  $user = $update["user_name"];                  
                  $desc = $update["repository"]["description"];
                  
                  if(strlen($desc) > 50 )
                  {
                     $desc = substr($desc,0,50).'...';
                  }
                  
                  $reply = ( $user .' Created a  tag '.PHP_EOL.'Ref :'.$update["ref"].PHP_EOL.$desc.PHP_EOL.PHP_EOL.$giturl ) ;
                  $this->telegramreceiver->sendreply($reply,$chatid ,"",null);
                  
                break;
                
                   
                case "note":                                    
                
                  $user = $update["user"]["name"];         
                  $desc = $update["repository"]["description"];
                  $noteeurl = $update["object_attributes"]["url"];
                  $note = $update["object_attributes"]["note"]; 
                  
                  if(strlen($desc) > 50 )
                  {
                     $desc = substr($desc,0,50).'...';
                  }
                  
                  $reply = ( $user .' Created a  note/comment '.PHP_EOL.'Title : '.PHP_EOL.$note.PHP_EOL.PHP_EOL.$desc.PHP_EOL.$noteeurl.PHP_EOL.PHP_EOL.$giturl) ;
                  $this->telegramreceiver->sendreply($reply,$chatid ,"",null);
                  
                break;
            }
          
          
          }
          else
          {
          echo 'no content from webhook' ;
          }   
         
        }
        
        //to find what is pushed last and save last output. Set foleder and subfolder chmod -R 777 <foldername/> with write permission
        
       /* $myfile = fopen("./log/log.txt", "w") or die("Unable to open file!");
        $txt =$update ;
        fwrite($myfile, print_r($commitsection,true));
        fclose($myfile);*/
        
  }
  
}
