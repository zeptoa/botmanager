<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Telegram extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()	
    {

        $content = file_get_contents("php://input");
        $update = json_decode($content, true);
        $this->load->database();
        $this->load->model('Datamodel'); 
        $this->load->library('telegramreceiver');     
        $r = $this->telegramreceiver->getbasic($update);
        $chatid = $r['chatid'];          
        
        $c = 0 ;
        $c = $this->Datamodel->count_users($chatid);
            
        if($c==0)
        {
        $this->Datamodel->adduser($r);
        }else
        {
        // $hd = $this->Datamodel->gethookid($chatid);         
        } 
        
        $this->prepare_reply($r);
        $this->Datamodel->addlog($r);
       
	}
    
    public function prepare_reply($list)
    {
        
        $this->load->library('telegramreceiver');     
        $this->load->model('Datamodel'); 
             
        $replymsg = '' ;
        $rcvdmsg = $list['message'];
        $chatid = $list['chatid'];        
       
        //$webhook = $hookid[0]->hookid;  

        if(!empty($rcvdmsg ))
        { 
        
        switch ($rcvdmsg) 
        {
            case "/start":
            
                $replymsg = '<b>Welcome !!! </b> '.$list["personid"].PHP_EOL.'The bot is started and we registered your ID . You can get your web-hook id by /config. Please add web-hook URL to your Gitlab setting. Please note that currently only push, merge request comments/notes and issues are supported .' ;
                break;
                
            case "/stop":
            
                $replymsg = 'OK Next please removes me from groups or mute notification.' ;                
                break;
                
            case "/config":
                
                $hd = $this->Datamodel->gethookid($chatid);
                
                if(!empty($hd)) 
                {                           
                $replymsg = 'Add the following web hook to you Gitlab settings > Webhooks'.PHP_EOL.'https://bot.zeptoa.com/gitlab/webhook/'. $hd  ;
                }else
                {
                $replymsg = 'Please do /start to register your id' ;
                }
                                
                break;
                
            default:    
            $replymsg = '' ;
            break;
        }      
                            
        if(!empty($replymsg) )
        {
        $this->telegramreceiver->sendreply($replymsg,$list['chatid'],$list['personid'],null);
        }
        
        }

    }
    
    
    function test($chatid)
    {
    $this->load->library('telegramreceiver');     
    $this->load->model('Datamodel'); 
    $hookid = $this->Datamodel->gethookid($chatid);
    echo $hookid[0]->hookid;  
   
    
    }

      

}
